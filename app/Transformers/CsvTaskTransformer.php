<?php

namespace App\Transformers;

use App\Models\CsvTask;
use League\Fractal\TransformerAbstract;

class CsvTaskTransformer extends TransformerAbstract
{
    /**
     * @param \App\CsvTask $csvTask
     * @return array
     */
    public function transform(CsvTask $csvTask)
    {
        return [
            'id' => (int) $csvTask->id,
            'filename' => (string) $csvTask->filename,
            'status' => (string) $csvTask->status,
            'created_at' => (string)$csvTask->created_at.' ('. \Carbon\Carbon::parse($csvTask->created_at)->diffForHumans().')',

        ];
    }
}
