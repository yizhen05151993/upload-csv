<?php

namespace App\Imports;

use App\Models\csv_upload;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Concerns\WithEvents;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithUpsertColumns;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

HeadingRowFormatter::default('none');

class CsvImport implements ToModel, WithHeadingRow, WithBatchInserts, WithUpserts, ShouldQueue, WithChunkReading, WithUpsertColumns
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new csv_upload([
            'unique_key' => $row['UNIQUE_KEY'],
            'product_title' => $row['PRODUCT_TITLE'],
            'product_description' => $row['PRODUCT_DESCRIPTION'],
            'style' => $row['STYLE#'],
            'sanmar_mainframe_color' => $row['SANMAR_MAINFRAME_COLOR'],
            'size' => $row['SIZE'],
            'color_name' => $row['COLOR_NAME'],
            'piece_price' => $row['PIECE_PRICE']
        ]);
    }

    public function upsertColumns()
    {
        return ['product_title', 'product_title', 'product_description', 'style', 'sanmar_mainframe_color', 'sanmar_mainframe_color', 'size', 'color_name', 'piece_price'];
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function uniqueBy()
    {
        return 'unique_key';
    }

}
