<?php

namespace App\Jobs;

use App\Models\CsvTask;
use Illuminate\Bus\Queueable;
use Yajra\DataTables\DataTables;
use Illuminate\Queue\SerializesModels;
use App\Transformers\CsvTaskTransformer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class NotifyUserOfCompletedImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $task;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->task = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        CsvTask::where('id',$this->task)->update(['status' => 'Completed']);

        $model = CsvTask::query()->orderBy('id', 'DESC'); // reverse due to

        $data = DataTables::of($model)
            ->setTransformer(new CsvTaskTransformer)
            ->toJson();
        event (new \App\Events\RefreshList($data));
    }
}
