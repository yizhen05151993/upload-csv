<?php

namespace App\Http\Controllers;

use Excel;
use App\Models\CsvTask;
use App\Imports\CsvImport;
use App\Jobs\ProductCSVData;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Storage;
use App\Transformers\CsvTaskTransformer;
use App\Jobs\NotifyUserOfCompletedImport;

class UploadController extends Controller
{

    public function list(){
        $model = CsvTask::query()->orderBy('id', 'DESC');

        return DataTables::of($model)
            ->setTransformer(new CsvTaskTransformer)
            ->toJson();
    }

    public function upload(Request $request)
    {

        if($request->file('file') ) {
            // insert task
            $a = new CsvTask();
            $a->filename = $request->file->getClientOriginalName();
            $a->status = 'Processing';
            $a->save();

            $related = $a->id;


            try {
                Excel::queueImport(new CsvImport, $request->file('file'))->chain([
                    new NotifyUserOfCompletedImport($related),
                ]);
            } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
                CsvTask::where('id',$related)->update(['status' => 'Failed']);
            }

            $model = CsvTask::query()->orderBy('id', 'DESC');

            return DataTables::of($model)
                ->setTransformer(new CsvTaskTransformer)
                ->toJson();
        }
    }
}
