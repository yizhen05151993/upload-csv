<?php

use Illuminate\Http\Request;
use App\Http\Controllers\UploadController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\ApiAuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
    //test
});

Route::group(['middleware' => ['cors', 'json.response']], function () {
    // public routes
    Route::post('/upload',[UploadController::class, 'upload'])->name('upload.api');
    Route::get('/list',[UploadController::class, 'list'])->name('list.api');

});
