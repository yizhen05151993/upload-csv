### Setup Project with docker
run the code below 
```
docker compose up -d
npm install
sh shell.sh
composer install
php artisan migrate
```

In your .env file, change the BROADCAST_DRIVER to pusher:
```
BROADCAST_DRIVER=pusher
```

Next in your .env file, set your Pusher details:
```
PUSHER_APP_ID=12345
PUSHER_APP_KEY=ABCDEFG
PUSHER_APP_SECRET=HIJKLMNOP
PUSHER_APP_CLUSTER=mt1
```

### Running your websocket + queue
```
php artisan websockets:serve
php artisan queue:work
