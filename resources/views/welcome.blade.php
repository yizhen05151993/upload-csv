<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<div class="container">
    {{-- <form method="post" action="{{ route('upload.api') }}" enctype="multipart/form-data"> --}}
    @csrf
    <div class="upload_container">
        <input type="file" name="file" id="file" accept=".csv" style="display:none;">
        <p>Select file/Drag and drop</p>
        <button id="uploadfile">Upload File</button>
    </div>
    {{-- </form> --}}

    <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="th-sm">Time
                </th>
                <th class="th-sm">File Name
                </th>
                <th class="th-sm">Status
                </th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

</html>
<script src="{{ asset('js/app.js') }}"></script>
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<script>
    $(document).ready(function () {
        console.log('init');
        checkFile();

        // preventing page from redirecting
        $("html").on("dragover", function (e) {
            e.preventDefault();
            e.stopPropagation();
            $("h1").text("Drag here");
        });

        $("html").on("drop", function (e) {
            e.preventDefault();
            e.stopPropagation();
        });

        // $('#dtBasicExample').DataTable();
        $('.dataTables_length').addClass('bs-select');

        // Drag enter
        $('.upload_container').on('dragenter', function (e) {
            e.stopPropagation();
            e.preventDefault();
        });

        // Drag over
        $('.upload_container').on('dragover', function (e) {
            e.stopPropagation();
            e.preventDefault();
        });

        // Drop
        $('.upload_container').on('drop', function (e) {
            e.stopPropagation();
            e.preventDefault();

            var file = e.originalEvent.dataTransfer.files;
            var fd = new FormData();

            fd.append('file', file[0]);

            uploadData(fd);
        });

        // Open file selector on div click
        $("#uploadfile").click(function () {
            $("#file").click();
        });

        // file selected
        $("#file").change(function () {
            var fd = new FormData();

            var files = $('#file')[0].files[0];
            fd.append('file', files);

            uploadData(fd);
        });

        Echo.channel('list').listen('RefreshList', (e) => {
            console.log(e);
            var items = e.list.original.data;
            console.log(items);
            $('#dtBasicExample').dataTable().fnDestroy();
            $('#dtBasicExample').DataTable({
                "order": [],
                "aaData": items,
                columns: [{
                        data: 'created_at',
                        name: 'created_at',
                        orderable: false
                    },
                    {
                        data: 'filename',
                        name: 'filename',
                        searchable: true
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },

                ]});
        })

    });


    function uploadData(formdata) {
        $("#file").val(null);
        $.ajax({
            url: "{{ route('upload.api') }}",
            type: 'post',
            data: formdata,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (response) {
                console.log(response);
                $('#dtBasicExample').dataTable().fnDestroy();
                console.log('destory and calling');
                checkFile();
            }
        });

    }

    function checkFile() {
        var table = $('#dtBasicExample').DataTable({
            processing: true,
            serverSide: true,
            serverMethod: 'get',
            ajax: "{{ route('list.api') }}",
            columns: [{
                    data: 'created_at',
                    name: 'created_at',
                },
                {
                    data: 'filename',
                    name: 'filename',
                    searchable: true
                },
                {
                    data: 'status',
                    name: 'status'
                },

            ]
        });
    }
</script>

<style>
    .upload_container {
        border: 1px solid;
        padding: 10px;
        display: grid;
        grid-template-columns: 50% 50%;
        margin-bottom: 50px;
        margin-top: 100px;
    }

</style>
