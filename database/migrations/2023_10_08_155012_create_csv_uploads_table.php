<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCsvUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csv_uploads', function (Blueprint $table) {
            $table->id();
            $table->string('unique_key',255);
            $table->string('product_title', 255);
            $table->text('product_description');
            $table->string('style', 255);
            $table->string('sanmar_mainframe_color', 255);
            $table->string('size', 255);
            $table->string('color_name', 255);
            $table->double('piece_price', 20,2);
            $table->timestamps();

            $table->unique(['unique_key']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csv_uploads');
    }
}
